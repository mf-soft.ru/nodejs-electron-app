// Добавляем к главному скрипту модуль electro
const electron = require('electron');
// Создаем переменную app от модуля electron
const app = electron.app;
// Создаем переменную BrowserWindow
const BrowserWindow = electron.BrowserWindow;

// Добавляем к главному скрипту модули path и url
const path = require('path');
const url = require('url');

// Инициализируем переменную отвечающую за главное окно приложения
let mainWindow;

// Метод создания окна
function createWindow() {
  // Присваиваем переменной объект BrowserWindow с размерами будущего окна
  mainWindow = new BrowserWindow({width: 800, height: 600});

  // Подгружаем к нашему окну страницу с дальнейшей логикой
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }));

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  // Создаем событие на закрытие окна
  mainWindow.on('closed', function () {
    mainWindow = null;
  });
}

// Добавляем обработчик события при готовности работы модуля на создание главного окна
app.on('ready', createWindow);

// Quit when all windows are closed.
// Добавляем обработчик события при закрытии всех окон
app.on('window-all-closed', function() {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

// Добавляем обработчик события при открытии окна
app.on('activate', function() {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  // Проверяем окно на существование
  if (mainWindow === null) {
    // Создаем окно
    createWindow();
  }
});